# 404 - Page not Found #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://relaxed-lovelace-c327dc.netlify.app/) | [Solution](https://devchallenges.io/solutions/1vgSBvc99SKhPXAAYoiW) | [Challenge](https://devchallenges.io/challenges/wBunSb7FPrIepJZAg0sY) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252F404PageThumbnail.png%3Falt%3Dmedia%26token%3D81f7c567-c0bc-446f-a549-07eeda31c5ef&w=750&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/wBunSb7FPrIepJZAg0sY) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)

## Features

This project features a desktop view and a mobile view of the page.


## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
